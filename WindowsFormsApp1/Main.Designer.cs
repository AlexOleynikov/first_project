﻿namespace WindowsFormsApp1
{
    partial class Main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.inComeButton = new System.Windows.Forms.Button();
            this.applyIncomeButton = new System.Windows.Forms.Button();
            this.inComeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.outCome = new System.Windows.Forms.Button();
            this.History = new System.Windows.Forms.Button();
            this.outComeNumericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.applyOutcomeButton = new System.Windows.Forms.Button();
            this.CurrentMoney = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.inComeNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.outComeNumericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // inComeButton
            // 
            this.inComeButton.Location = new System.Drawing.Point(12, 12);
            this.inComeButton.Name = "inComeButton";
            this.inComeButton.Size = new System.Drawing.Size(168, 33);
            this.inComeButton.TabIndex = 0;
            this.inComeButton.Text = "Положить";
            this.inComeButton.UseVisualStyleBackColor = true;
            this.inComeButton.Click += new System.EventHandler(this.Button1_Click);
            // 
            // applyIncomeButton
            // 
            this.applyIncomeButton.Location = new System.Drawing.Point(312, 12);
            this.applyIncomeButton.Name = "applyIncomeButton";
            this.applyIncomeButton.Size = new System.Drawing.Size(168, 33);
            this.applyIncomeButton.TabIndex = 1;
            this.applyIncomeButton.Text = "Подтвердить";
            this.applyIncomeButton.UseVisualStyleBackColor = true;
            this.applyIncomeButton.Visible = false;
            this.applyIncomeButton.Click += new System.EventHandler(this.ApplyIncomeButton_Click);
            // 
            // inComeNumericUpDown
            // 
            this.inComeNumericUpDown.Location = new System.Drawing.Point(186, 25);
            this.inComeNumericUpDown.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.inComeNumericUpDown.Name = "inComeNumericUpDown";
            this.inComeNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.inComeNumericUpDown.TabIndex = 3;
            this.inComeNumericUpDown.Visible = false;
            // 
            // outCome
            // 
            this.outCome.Location = new System.Drawing.Point(12, 62);
            this.outCome.Name = "outCome";
            this.outCome.Size = new System.Drawing.Size(168, 37);
            this.outCome.TabIndex = 4;
            this.outCome.Text = "Снять";
            this.outCome.UseVisualStyleBackColor = true;
            this.outCome.Click += new System.EventHandler(this.OutCome_Click);
            // 
            // History
            // 
            this.History.Location = new System.Drawing.Point(12, 105);
            this.History.Name = "History";
            this.History.Size = new System.Drawing.Size(168, 39);
            this.History.TabIndex = 5;
            this.History.Text = "История";
            this.History.UseVisualStyleBackColor = true;
            this.History.Click += new System.EventHandler(this.History_Click);
            // 
            // outComeNumericUpDown1
            // 
            this.outComeNumericUpDown1.Location = new System.Drawing.Point(186, 79);
            this.outComeNumericUpDown1.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.outComeNumericUpDown1.Name = "outComeNumericUpDown1";
            this.outComeNumericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.outComeNumericUpDown1.TabIndex = 6;
            // 
            // applyOutcomeButton
            // 
            this.applyOutcomeButton.Location = new System.Drawing.Point(312, 62);
            this.applyOutcomeButton.Name = "applyOutcomeButton";
            this.applyOutcomeButton.Size = new System.Drawing.Size(168, 37);
            this.applyOutcomeButton.TabIndex = 7;
            this.applyOutcomeButton.Text = "Подтвердить";
            this.applyOutcomeButton.UseVisualStyleBackColor = true;
            this.applyOutcomeButton.Click += new System.EventHandler(this.ApplyOutcomeButton_Click);
            // 
            // CurrentMoney
            // 
            this.CurrentMoney.AutoSize = true;
            this.CurrentMoney.Font = new System.Drawing.Font("Impact", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CurrentMoney.Location = new System.Drawing.Point(210, 142);
            this.CurrentMoney.Name = "CurrentMoney";
            this.CurrentMoney.Size = new System.Drawing.Size(81, 34);
            this.CurrentMoney.TabIndex = 8;
            this.CurrentMoney.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 185);
            this.Controls.Add(this.CurrentMoney);
            this.Controls.Add(this.applyOutcomeButton);
            this.Controls.Add(this.outComeNumericUpDown1);
            this.Controls.Add(this.History);
            this.Controls.Add(this.outCome);
            this.Controls.Add(this.inComeNumericUpDown);
            this.Controls.Add(this.applyIncomeButton);
            this.Controls.Add(this.inComeButton);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.inComeNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.outComeNumericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button inComeButton;
        private System.Windows.Forms.Button applyIncomeButton;
        private System.Windows.Forms.NumericUpDown inComeNumericUpDown;
        private System.Windows.Forms.Button outCome;
        private System.Windows.Forms.Button History;
        private System.Windows.Forms.NumericUpDown outComeNumericUpDown1;
        private System.Windows.Forms.Button applyOutcomeButton;
        private System.Windows.Forms.Label CurrentMoney;
    }
}

