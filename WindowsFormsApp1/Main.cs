﻿using AccountedServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    public partial class Main : Form
    {
        Service service;
        public Main()
        {
            service = new Service();
            InitializeComponent();
            UpdateCurrentMoney();
            SetInVisible();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            inComeNumericUpDown.Value = 0;
            inComeNumericUpDown.Visible = true;
            applyIncomeButton.Visible = true;
            applyOutcomeButton.Visible = false;
            outComeNumericUpDown1.Visible = false;

        }

        private void DisableNavBar()
        {
            inComeButton.Enabled = false;

        }

        private void SetInVisible()

        {
            inComeNumericUpDown.Visible = false;
            applyIncomeButton.Visible = false;
            applyOutcomeButton.Visible = false;
            outComeNumericUpDown1.Visible = false;

        }

        private void ApplyIncomeButton_Click(object sender, EventArgs e)
        {         
            

                try
                {
                    var money = service.InComeMoney((int)inComeNumericUpDown.Value);
                //Console.WriteLine("операция прошла успешно");
                MessageBox.Show("операция прошла успешно");
                    //Console.WriteLine("Остаток на счете " + money);
                }
                catch (Exception eEx)
                {
                MessageBox.Show(eEx.Message);

                }
            SetInVisible();
            UpdateCurrentMoney();




        }

        private void UpdateCurrentMoney()

        {

            CurrentMoney.Text = service.Kazna().ToString();

        }

        private void OutCome_Click(object sender, EventArgs e)
        {

            outComeNumericUpDown1.Value = 0;
            outComeNumericUpDown1.Visible = true;
            applyOutcomeButton.Visible = true;
            inComeNumericUpDown.Visible = false;
            applyIncomeButton.Visible = false;
            
        }

        private void ApplyOutcomeButton_Click(object sender, EventArgs e)
        {
            try
            {
                var money = service.GetMoney((int)outComeNumericUpDown1.Value);
                //Console.WriteLine("операция прошла успешно");
                MessageBox.Show("операция прошла успешно");
                //Console.WriteLine("Остаток на счете " + money);
            }
            catch (Exception eEx)
            {
                MessageBox.Show(eEx.Message);

            }
            SetInVisible();
            UpdateCurrentMoney();
        }

        private void History_Click(object sender, EventArgs e)
        {
            var histories = service.GetHistory();
            ForHistory newForm = new ForHistory(histories);
            newForm.Show();

        }


    }
}
