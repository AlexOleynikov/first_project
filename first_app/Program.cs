﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccountedServices;



namespace first_app
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("открываем новый счет");
            var service = new Service();

            while (true)
            {
                Console.WriteLine("Положить(I)/снять(O)/Вывести историю(H)/Казна(K)");

                var command = Console.ReadLine().ToUpper();
                if (command == "I")
                {
                    Console.WriteLine("сколько?!");
                    var inCome = Console.ReadLine();
                    int inComeInt;

                    if  (int.TryParse(inCome,out inComeInt))
                    {
                        try
                        {
                            var money =  service.InComeMoney(inComeInt);
                            Console.WriteLine("операция прошла успешно");
                            Console.WriteLine("Остаток на счете " + money);
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine(e.Message);

                        }
                    }
                    else
                    {
                        Console.WriteLine("Ты не прав");

                    }
                        

                }

                if (command == "O")
                {
                    Console.WriteLine("сколько?!");
                    var outCome = Console.ReadLine();
                    int outComeInt;
                    
                    if (int.TryParse(outCome,out outComeInt))
                    {
                        try
                        {
                             var money = service.GetMoney(outComeInt);
                            Console.WriteLine("операция прошла успешно");
                            Console.WriteLine("Остаток на счете " + money);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);

                        }
                    }
                    else
                    {
                        Console.WriteLine("Введите число");

                    }
                        
                }

                if (command == "H")
                {
                    try
                    {

                        var history = service.GetHistory();
                        foreach (var h in history)
                        {
                            Console.WriteLine(h);
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);

                    }
                }

                if (command == "K")
                {

                    int m = service.Kazna();

                    Console.WriteLine("Остаток на счете " +  m);


                }
            }

        }
    }
}
