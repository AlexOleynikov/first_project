﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using DataBase.Models;
using Newtonsoft.Json;

namespace DataBase
{
    public class JsonDataBase : IDataBase
    {
        const string FileName = "DataBase.Json";

        public List<History> ReadHistory()
        {
            var read = ReadFromFile();
            if (read.Histories == null)
            {
                return new List<History>();

            }
            else
            {

                return read.Histories;

            }
        }

        public int ReadMoney()
        {
            
            var read = ReadFromFile();
            return read.Money;
        }

        public void WriteHistory(History history)
        {
            var write = ReadFromFile();
            if (write.Histories == null)
            {
                write.Histories = new List<History>();

            }

            write.Histories.Add(history);
            WriteToFile(write);
        }

        public void WriteMoney(int money)
        {
            var check = ReadFromFile();
            check.Money = money;
            WriteToFile(check);


        }

        void CreateFileIfNotExists()
        {
            if (!File.Exists(FileName))
            {
                var fs = File.Create(FileName);
                fs.Dispose();
            }
            
        }

        Check ReadFromFile()
        {

            CreateFileIfNotExists();
            string text = File.ReadAllText(FileName);
            var result = JsonConvert.DeserializeObject<Check>(text) ?? new Check();
            return result;

        }

        void WriteToFile(Check check)
        {
            CreateFileIfNotExists();
            string text = JsonConvert.SerializeObject(check);
            File.WriteAllText(FileName, text);


        }
    }
}
