﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBase.Models
{
    public class Check
    {
        public int Money { get; set; }
        public List<History> Histories { get; set; }

    }
}
