﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBase
{
    public class History
    {
        public int Value { get; set; }
        public DateTime DateTime { get; set; }
        public OperationType OperationType { get; set; }

    }
    public enum OperationType {InCome,OutCome}
}
