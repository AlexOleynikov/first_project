﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBase
{
    public interface IDataBase
    {
        List<History> ReadHistory();
        void WriteHistory(History history);
        int ReadMoney();
        void WriteMoney(int money);
            

        
    }


    
}
